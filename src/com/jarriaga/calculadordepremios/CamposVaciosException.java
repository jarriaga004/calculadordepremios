package com.jarriaga.calculadordepremios;

public class CamposVaciosException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public CamposVaciosException() {
		super();
	}

	public CamposVaciosException(String detailMessage, Throwable throwable) {
		super(detailMessage, throwable);
	}

	public CamposVaciosException(String detailMessage) {
		super(detailMessage);
	}

	public CamposVaciosException(Throwable throwable) {
		super(throwable);
	}

}
