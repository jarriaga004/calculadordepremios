package com.jarriaga.calculadordepremios;

import java.io.IOException;
import java.net.MalformedURLException;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

import android.os.Bundle;
import android.os.StrictMode;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.Menu;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends Activity {

	static double jugado, ganado;
	static boolean noVolverAMostrar = false;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
		StrictMode.setThreadPolicy(policy);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		try {
			String urlPremios = "http://api.elpais.com/ws/LoteriaNavidadPremiados?n=resumen";
			JSONObject json = new JSONObject();
			json = getJSONRespuesta(urlPremios, 8);// premios= tiene 8 letras
			TextView tv;
			if (json != null) {

				if (json.getInt("numero1") != -1) {
					tv = (TextView) this.findViewById(R.id.numero1);
					tv.setText(String.valueOf(json.getInt("numero1")));
				}
				if (json.getInt("numero2") != -1) {
					tv = (TextView) this.findViewById(R.id.numero2);
					tv.setText(String.valueOf(json.getInt("numero2")));
				}
				if (json.getInt("numero3") != -1) {
					tv = (TextView) this.findViewById(R.id.numero3);
					tv.setText(String.valueOf(json.getInt("numero3")));
				}
				if (json.getInt("numero4") != -1) {
					tv = (TextView) this.findViewById(R.id.numero4);
					tv.setText(String.valueOf(json.getInt("numero4")));
				}
				if (json.getInt("numero5") != -1) {
					tv = (TextView) this.findViewById(R.id.numero5);
					tv.setText(String.valueOf(json.getInt("numero5")));
				}
				if (json.getInt("numero6") != -1) {
					tv = (TextView) this.findViewById(R.id.numero6);
					tv.setText(String.valueOf(json.getInt("numero6")));
				}
				if (json.getInt("numero7") != -1) {
					tv = (TextView) this.findViewById(R.id.numero7);
					tv.setText(String.valueOf(json.getInt("numero7")));
				}
				if (json.getInt("numero8") != -1) {
					tv = (TextView) this.findViewById(R.id.numero8);
					tv.setText(String.valueOf(json.getInt("numero8")));
				}
				if (json.getInt("numero9") != -1) {
					tv = (TextView) this.findViewById(R.id.numero9);
					tv.setText(String.valueOf(json.getInt("numero9")));
				}
				if (json.getInt("numero10") != -1) {
					tv = (TextView) this.findViewById(R.id.numero10);
					tv.setText(String.valueOf(json.getInt("numero10")));
				}
				if (json.getInt("numero11") != -1) {
					tv = (TextView) this.findViewById(R.id.numero11);
					tv.setText(String.valueOf(json.getInt("numero11")));
				}
				if (json.getInt("numero12") != -1) {
					tv = (TextView) this.findViewById(R.id.numero12);
					tv.setText(String.valueOf(json.getInt("numero12")));
				}
				if (json.getInt("numero13") != -1) {
					tv = (TextView) this.findViewById(R.id.numero13);
					tv.setText(String.valueOf(json.getInt("numero13")));
				}
			} else {
				throw new SinInternetException();
			}
		} catch (JSONException e) {
			e.printStackTrace();
		} catch (SinInternetException e) {
			final AlertDialog dialog = new AlertDialog.Builder(new ContextThemeWrapper(this, android.R.style.Theme_Light))
			.create();
			dialog.setButton(AlertDialog.BUTTON_POSITIVE, "Aceptar", new OnClickListener() {

				@Override
				public void onClick(DialogInterface dialog, int which) {
					dialog.dismiss();
				}
			});
			dialog.setTitle("Error al establecer la conexi�n. Compruebe que tiene conexi�n a Internet");
			dialog.show();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_main, menu);
		return true;
	}

	public void calcular(View view) {
		double lastJugado = 0;
		double lastGanado = 0;
		int numero;
		try {
			EditText et = (EditText) getWindow().findViewById(R.id.editTextCantidad);
			if (et.getText().toString().equals(""))
				throw new CamposVaciosException();
			lastJugado = Double.parseDouble(et.getText().toString().replace(",","."));
			et = (EditText) getWindow().findViewById(R.id.editTextNumero);
			if (et.getText().toString().equals(""))
				throw new CamposVaciosException();
			numero = Integer.parseInt(et.getText().toString());
			String url = "http://api.elpais.com/ws/LoteriaNavidadPremiados?s=1&n=" + numero;
			JSONObject json = getJSONRespuesta(url, 9); // busqueda= tiene 9
														// letras
			if (json == null)
				throw new SinInternetException();
			jugado += lastJugado;

			lastGanado = json.getInt("premio") * (lastJugado / 20);
			ganado += lastGanado;
			TextView tv = (TextView) getWindow().findViewById(R.id.textViewTotalJugada);
			tv.setText("Cantidad total jugada: " + jugado + " �");
			tv = (TextView) getWindow().findViewById(R.id.textViewTotalGanada);
			tv.setText("Cantidad total ganada " + ganado + " �");

			if (!noVolverAMostrar) {
				final AlertDialog dialog = new AlertDialog.Builder(new ContextThemeWrapper(this, android.R.style.Theme_Light))
						.create();
				int status = json.getInt("status");
				switch (status) {
				case 0: {
					dialog.setTitle("El sorteo no ha comenzado a�n");
					break;
				}
				case 1: {
					dialog.setTitle("El sorteo ha empezado. Los premios pueden tardar en aparecer");
					break;
				}
				case 2: {
					dialog.setTitle("El sorteo ha terminado. Los datos son tomados a o�do");
					break;
				}
				case 3: {
					dialog.setTitle("El sorteo ha terminado y existe un PDF oficial");
					break;
				}
				case 4: {
					dialog.setTitle("El sorteo ha terminado y los datos de la app son oficiales");
				}
				}
				dialog.setButton(AlertDialog.BUTTON_POSITIVE, "Aceptar", new OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
					}
				});
				dialog.setButton(AlertDialog.BUTTON_NEUTRAL, "No volver a mostrar", new OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
						noVolverAMostrar = true;
					}
				});
				dialog.show();
			}
		} catch (JSONException e) {
			e.printStackTrace();
		} catch (CamposVaciosException e) {
			e.printStackTrace();
		} catch (SinInternetException e) {
			e.printStackTrace();
			final AlertDialog dialog = new AlertDialog.Builder(new ContextThemeWrapper(this, android.R.style.Theme_Light))
			.create();
			dialog.setButton(AlertDialog.BUTTON_POSITIVE, "Aceptar", new OnClickListener() {

				@Override
				public void onClick(DialogInterface dialog, int which) {
					dialog.dismiss();
				}
			});
			dialog.setTitle("Error al establecer la conexi�n. Compruebe que tiene conexi�n a Internet");
		}
	}

	private JSONObject getJSONRespuesta(String url, int longitudNombreJSON) {
		JSONObject jsonObject = null;
		HttpClient httpClient = null;
		try {
			Log.i("URL de WS", url);
			httpClient = new DefaultHttpClient();
			HttpGet request = new HttpGet(url);
			BasicResponseHandler responseHandler = new BasicResponseHandler();
			String responseBody = httpClient.execute(request, responseHandler);
			Log.i("String recibido", responseBody);
			responseBody = responseBody.substring(longitudNombreJSON);
			httpClient.getConnectionManager().shutdown();
			jsonObject = new JSONObject(responseBody);
			Log.i("JSON devuelto por WS", jsonObject.toString());
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return jsonObject;
	}
}
