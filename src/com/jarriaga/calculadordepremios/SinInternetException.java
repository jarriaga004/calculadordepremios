package com.jarriaga.calculadordepremios;

public class SinInternetException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public SinInternetException() {
		super();
	}

	public SinInternetException(String detailMessage, Throwable throwable) {
		super(detailMessage, throwable);
	}

	public SinInternetException(String detailMessage) {
		super(detailMessage);
	}

	public SinInternetException(Throwable throwable) {
		super(throwable);
	}

}
